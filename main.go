package main

import (
  "gitlab.com/ciwee/back-end/shop/back-end-for-front-end/src"
)

func main() {
  new(src.Application).Init()
}
