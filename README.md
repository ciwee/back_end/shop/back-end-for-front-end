# Graphql na criação de projeto

- comando para configurar o graphql
  ```shell
  go get github.com/99designs/gqlgen
  ```
- criar o arquivo [gplgen.yml](gqlgen.yml)
    - obs.: esse arquivo case não será alterado
- criar o arquivo [tools.go](tools.go)
- criar a pasta ./graph
- criar arquivo [./graph/schema.graphqls](./graph/schema.graphqls)
- comando para atualizar o schema graphql
  ```shell
  go mod tidy
  go run github.com/99designs/gqlgen generate
  ```
  https://www.apollographql.com/blog/graphql/golang/using-graphql-with-golang/


## Compilara os arquivos proto
```shell
protoc --proto_path=proto proto/*.proto --go_out=plugins=grpc:protobuffer
```

# Instalar o Evans para teste o servidor grpc
https://github.com/ktr0731/evans
```shell
go install github.com/ktr0731/evans@latest
```
## Comandos Evans
Mesma porta do arquivo cmd/server/main.go
```shell
evans -r -p 50055
service QrCodeService
```