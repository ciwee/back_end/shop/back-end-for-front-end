package framework_properties

import (
  "os"
  "regexp"
  "testing"

  "github.com/stretchr/testify/assert"
)

func Test_devera_carregar_as_propriedade_com_sucesso(t *testing.T) {

  assertions := assert.New(t)
  projectName := regexp.MustCompile(`^(.*` + "back-end-for-front-end" + `)`)
  currentWorkDirectory, _ := os.Getwd()
  rootPath := string(projectName.Find([]byte(currentWorkDirectory))) + "/.env-test"

  newProperties := NewProperties(rootPath)
  load := newProperties.Load()

  assertions.Equal("mongodb://admin:admin123@127.0.0.1:27017", load.DataBaseMongoUrl)
  assertions.Equal("8080", load.PortServer)

}
