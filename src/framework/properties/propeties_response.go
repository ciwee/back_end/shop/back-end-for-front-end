package framework_properties

type PropertiesResponse struct {
  DataBaseMongoUrl      string
  PortServer            string
  GrpcClientProductHost string
  GrpcClientProductPort string
}
