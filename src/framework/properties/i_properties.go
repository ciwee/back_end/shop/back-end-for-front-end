package framework_properties

type IProperties interface {
	Load() PropertiesResponse
}
