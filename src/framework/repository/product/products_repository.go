package repository_product

import (
  "context"
  "gitlab.com/ciwee/back-end/shop/back-end-for-front-end/protobuffer"
  "gitlab.com/ciwee/back-end/shop/back-end-for-front-end/src/framework/grpc"
  "gitlab.com/ciwee/back-end/shop/back-end-for-front-end/src/framework/properties"
)

type ProductRepository struct {
  grpcClient *grpc.GRPCClient
}

func (q ProductRepository) Create(request protobuffer.CreateProductInput) (*protobuffer.CreateProductOutput, error) {
  connection := q.grpcClient.Connect()
  defer q.grpcClient.Close()
  client := protobuffer.NewProductServiceClient(connection)
  return client.Create(context.Background(), &request)
}

func NewProductRepository(properties framework_properties.PropertiesResponse) ProductRepository {
  return ProductRepository{
    grpcClient: grpc.NewGRPCCliente(properties),
  }
}
