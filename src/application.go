package src

import (
  "github.com/labstack/echo/v4"
  "gitlab.com/ciwee/back-end/shop/back-end-for-front-end/src/framework/controller/graphql"
  properties "gitlab.com/ciwee/back-end/shop/back-end-for-front-end/src/framework/properties"
)

type Application struct {
}

func (a Application) Init() {
  propertiesResponse := properties.NewProperties("$GOPATH/.env").Load()

  //cardController := card.NewCardController()
  //catalogController := card.NewCatalogController()
  graphqlController := graphql.NewGraphqlController()

  e := echo.New()

  //e.POST("/cards/customers", cardController.Create)
  //e.GET("/cards/catalogs", catalogController.FindAll)
  e.POST("/query", graphqlController.Query)
  e.GET("/playground", graphqlController.Playground)

  e.Logger.Fatal(e.Start(":" + propertiesResponse.PortServer))
}
