package repository

import (
  "gitlab.com/ciwee/back-end/shop/back-end-for-front-end/protobuffer"
)

type IProductRepository interface {
  Create(request protobuffer.CreateProductInput) (*protobuffer.CreateProductOutput, error)
}
