package repository

import (
  "gitlab.com/ciwee/back-end/shop/back-end-for-front-end/graph/model"
  "gitlab.com/ciwee/back-end/shop/back-end-for-front-end/protobuffer"
)

type ProductAdapterRepository struct {
  productRepository IProductRepository
}

func (p *ProductAdapterRepository) Create(product model.CreateProductInput) (*model.CreateProductOutput, error) {

  request := protobuffer.CreateProductInput{
    Description: product.Description,
    ImageLink:   product.ImageLink,
    EnableGrid:  product.EnableGrid,
  }

  if len(product.Grids) > 0 {
    var grids []*protobuffer.CreateGridInput
    for _, item := range product.Grids {
      var itemsGrid []*protobuffer.CreateGridItemInput
      for _, itemGrid := range item.Items {
        itemsGrid = append(itemsGrid, &protobuffer.CreateGridItemInput{
          Description: itemGrid.Description,
          Acronym:     itemGrid.Acronym,
          Sequencer:   int64(itemGrid.Sequencer),
        })
      }
      grids = append(grids, &protobuffer.CreateGridInput{
        TypeGrid: item.TypeGrid,
        Items:    itemsGrid,
      })
    }
    request.Grids = grids
  }

  response, err := p.productRepository.Create(request)

  if err != nil {
    return nil, err
  }
  error := response.GetError()
  return &model.CreateProductOutput{
    Message: response.GetMessage(),
    Error:   &error,
  }, nil

}

func NewProductAdapterRepository(productRepository IProductRepository) IProductAdapterRepository {
  return &ProductAdapterRepository{
    productRepository: productRepository,
  }
}
