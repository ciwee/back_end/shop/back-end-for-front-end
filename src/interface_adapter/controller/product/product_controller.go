package product

import (
  "gitlab.com/ciwee/back-end/shop/back-end-for-front-end/graph/model"
  framework_properties "gitlab.com/ciwee/back-end/shop/back-end-for-front-end/src/framework/properties"
  "gitlab.com/ciwee/back-end/shop/back-end-for-front-end/src/framework/repository/product"
  "gitlab.com/ciwee/back-end/shop/back-end-for-front-end/src/interface_adapter/repository"
  "os"
  "regexp"
)

type ProductController struct {
  productAdapterRepository repository.IProductAdapterRepository
}

func (p *ProductController) Create(product model.CreateProductInput) (*model.CreateProductOutput, error) {
  return p.productAdapterRepository.Create(product)
}

func NewProductController() IProductController {
  projectName := regexp.MustCompile(`^(.*` + "back-end-for-front-end" + `)`)
  currentWorkDirectory, _ := os.Getwd()
  rootPath := string(projectName.Find([]byte(currentWorkDirectory))) + "/.env-test"

  propertiesResponse := framework_properties.NewProperties(rootPath).Load()
  productRepository := repository_product.NewProductRepository(propertiesResponse)
  productAdapterRepository := repository.NewProductAdapterRepository(productRepository)
  return &ProductController{
    productAdapterRepository: productAdapterRepository,
  }
}
