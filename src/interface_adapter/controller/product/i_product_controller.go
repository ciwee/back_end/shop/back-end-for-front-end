package product

import "gitlab.com/ciwee/back-end/shop/back-end-for-front-end/graph/model"

type IProductController interface {
  Create(product model.CreateProductInput) (*model.CreateProductOutput, error)
}
